package ictgradschool.industry.fileio.groupexercises;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class GroupEx03 {

    public Customer readCustomer(String fileName) {

        Customer customer = new Customer();
        File file = new File(fileName);

        // TODO Complete this (use a Scanner)
        System.out.println("Scanning with pipe-style delimiter");
        try (Scanner scanner = new Scanner(file)) {

            scanner.useDelimiter("\\|");

            while (scanner.hasNext()) {
                customer.setName(scanner.next());
                customer.setAge(scanner.nextInt());
                customer.setAddress(scanner.next());
                customer.setSubscribed(scanner.nextBoolean());
            }

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        return customer;
    }

    public static void main(String[] args) {

        GroupEx03 ex03 = new GroupEx03();

        Customer readCustomer = ex03.readCustomer("customer.txt");
        System.out.println("Customer details:");
        System.out.println(readCustomer);
    }
}
