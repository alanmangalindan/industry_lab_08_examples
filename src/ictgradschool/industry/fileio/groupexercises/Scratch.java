package ictgradschool.industry.fileio.groupexercises;

import java.io.File;

public class Scratch {

    public static void main(String[] args) {
        new Scratch().start();
    }

    public void start() {
        createFolder("myNewFolder");
    }

    public void createFolder(String folderName) {

        File theFolder = new File(folderName);

        if (theFolder.exists()) {
            System.out.println("Folder already exists");
        } else {
            System.out.println("Will create the folder");
            theFolder.mkdirs(); // mkdirs ensures the full folder structure is created. This is preferred over mkdir().
        }

    }
}
