package ictgradschool.industry.fileio.groupexercises;

import java.io.*;

public class GroupEx02 {

    public void writeCustomer(Customer customer, String fileName) {

        // TODO Complete this (use a DataOutputStream)
        File file = new File(fileName);

        try (DataOutputStream dOut = new DataOutputStream(new FileOutputStream(file))) {

            dOut.writeUTF(customer.getName());
            dOut.writeInt(customer.getAge());
            dOut.writeUTF(customer.getAddress());
            dOut.writeBoolean(customer.isSubscribed());

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    public Customer readCustomer(String fileName) {

        Customer customer = new Customer();

        // TODO Complete this (use a DataInputStream)
        File file = new File(fileName);
        try (DataInputStream dIn = new DataInputStream(new FileInputStream(file))) { //shift + F6 : rename variables

            customer.setName(dIn.readUTF());
            customer.setAge(dIn.readInt());
            customer.setAddress(dIn.readUTF());
            customer.setSubscribed(dIn.readBoolean());

        } catch (FileNotFoundException e) { // technically this is also an IOException and caught by the line below
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }


        return customer;
    }

    public static void main(String[] args) {

        GroupEx02 ex02 = new GroupEx02();

        Customer customer = new Customer("Bob", 42, "123 Some Street", true);

        String fileName = "customerFile.dat";
        ex02.writeCustomer(customer, fileName);

        Customer readCustomer = ex02.readCustomer(fileName);
        System.out.println("Customer details:");
        System.out.println(readCustomer);
    }
}
